class Owner < ActiveRecord::Base
  has_many :cars
  belongs_to :address
end
